FROM ruby:2.4.2
RUN apt-get update -qq && apt-get install -y build-essential
RUN mkdir /myapp
WORKDIR /myapp
ADD Gemfile /myapp/Gemfile
ADD Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
ADD ./myapp
EXPOSE 4567
CMD ["ruby", "blockchain.rb"]
