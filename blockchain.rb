require 'sinatra'
require 'uuid'
require 'digest'
require 'json'

class Blockchain
  attr_reader :current_transactions, :chain

  def initialize
    @current_transactions = []
    @chain = []

    new_block(1, 100)
  end

  def new_transaction(sender, recipient, amount)
    transaction = { :sender => sender, :recipient => recipient, :amount => amount }
    @current_transactions.push(transaction)
    transaction
  end

  def new_block(proof, previous_hash = nil)
    previous_hash ||= Blockchain::hash(last_block)
    block = {
      :index => @chain.length + 1,
      :timestamp => Time.now,
      :transactions => @current_transactions,
      :proof => proof,
      :previous_hash => previous_hash
    }
    @current_transactions = []
    @chain.push(block)
    block
  end

  def last_block
    @chain.last
  end

  def proof_of_work(last_proof)
    proof = 0
    until Blockchain::valid_proof(last_proof, proof) do
      proof = proof + 1
    end
    proof
  end

  def self.hash(block)
    Digest::SHA256.hexdigest block.to_json
  end

  def self.valid_proof(last_proof, proof)
    guess = sprintf('%s%d', last_proof, proof)
    guess_hash = Digest::SHA256.hexdigest guess
    guess_hash[0,4] == '0000'
  end
end

blockchain = Blockchain.new

get '/mine' do
  content_type :json
  last_proof = blockchain.last_block[:proof]
  proof = blockchain.proof_of_work last_proof

  uuid = UUID.new

  blockchain.new_transaction(0, uuid.generate.gsub('-', ''), 1)

  block = blockchain.new_block(proof)

  {
    :message => 'new block forged',
    :index => block[:index],
    :transactions => block[:transactions],
    :proof => block[:proof],
    :previous_hash => block[:previous_hash]
  }.to_json
end

post '/transactions/new' do
  content_type :json
  params_json = JSON.parse(request.body.read)

  ['sender', 'recipient', 'amount'].each do |param|
    unless params_json.key?(param)
      halt 400
    end
  end

  index = blockchain.new_transaction(params_json['sender'], params_json['recipient'], params_json['amount'])

  index.to_json
end

get '/chain' do
  content_type :json
  { :chain => blockchain.chain, :length => blockchain.chain.length }.to_json
end
